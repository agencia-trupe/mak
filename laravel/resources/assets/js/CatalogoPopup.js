export default function CatalogoPopup() {
    $('.catalogo-popup').fancybox({
        type: 'ajax',
        padding: 0
    });

    var envioCatalogo = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-catalogo-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/catalogo',
            data: {
                id: $('#id').val(),
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
            },
            success: function(data) {
                $('.catalogo-download-link a').attr('href', data.link);
                $form.fadeOut(function() {
                    $('.catalogo-download-link').fadeIn();
                });
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    $('body').on('submit', '#form-catalogo', envioCatalogo);
};
