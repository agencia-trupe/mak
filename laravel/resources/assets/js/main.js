import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import FormularioContato from './FormularioContato';
import AnexoCurriculo from './AnexoCurriculo';
import BannersHome from './BannersHome';
import BlogComentario from './BlogComentario';
import CertificadoPopup from './CertificadoPopup';
import CatalogoPopup from './CatalogoPopup';
import HomeCarousel from './HomeCarousel';

AjaxSetup();
MobileToggle();
FormularioContato();
AnexoCurriculo();
BannersHome();
BlogComentario();
CertificadoPopup();
CatalogoPopup();
HomeCarousel();
