export default function AnexoCurriculo() {
    $('#input-curriculo').change(function(event) {
        event.preventDefault();

        if(!event.target.files.length) {
            $(this).parent().removeClass('active');
            $(this).next('span').html('ANEXAR CURRÍCULO');
            return;
        }

        $(this).parent().addClass('active');
        $(this).next('span').html(event.target.files[0].name);
    });
};
