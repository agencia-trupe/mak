export default function BlogComentario() {
    var envioComentario = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-comentario-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/blog-comentario',
            data: {
                post: $('#post').val(),
                nome: $('#nome').val(),
                email: $('#email').val(),
                comentario: $('#comentario').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                if (data.success) $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    $('#form-comentario').on('submit', envioComentario);
};
