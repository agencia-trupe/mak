@extends('frontend.common.template')

@section('content')

    <div class="main empresa">
        <div class="center">
            <h2 class="titulo">EMPRESA</h2>

            <div class="texto">
                {!! $empresa->texto !!}
            </div>

            <div class="quadros">
                <div class="quadro">
                    <h3>MISSÃO</h3>
                    <p>{!! $empresa->missao !!}</p>
                </div>
                <div class="quadro">
                    <h3>VISÃO</h3>
                    <p>{!! $empresa->visao !!}</p>
                </div>
                <div class="quadro">
                    <h3>VALORES</h3>
                    <p>{!! $empresa->valores !!}</p>
                </div>
            </div>

            <div class="imagens">
                @foreach(range(1,3) as $i)
                    <img src="{{ asset('assets/img/empresa/'.$empresa->{'imagem_'.$i}) }}" alt="">
                @endforeach
            </div>
        </div>

        <div class="evolucao-marca">
            <div class="center">
                <h3>EVOLUÇÃO DA MARCA MAK PAINÉIS</h3>
                @foreach($marcas as $marca)
                    <div class="marca">
                        <img src="{{ asset('assets/img/evolucao-da-marca/'.$marca->imagem) }}" alt="">
                        <span>{{ $marca->ano }}</span>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="evolucao-catalogo">
            <div class="center">
                <h3>EVOLUÇÃO DOS CATÁLOGOS MAK PAINÉIS</h3>
                @foreach($catalogos as $catalogo)
                    <div class="catalogo">
                        <img src="{{ asset('assets/img/evolucao-dos-catalogos/'.$catalogo->imagem) }}" alt="">
                    </div>
                @endforeach
            </div>
        </div>

        <div class="certificados">
            <div class="center">
                <h3>CERTIFICADOS E HOMOLOGAÇÕES</h3>
                @foreach($certificados as $certificado)
                    @if($certificado->texto)
                    <a href="#certificado-{{ $certificado->id }}" class="certificado certificado-btn">
                        <img src="{{ asset('assets/img/certificados-e-homologacoes/'.$certificado->imagem) }}" alt="">
                        <span>{{ $certificado->nome }}</span>
                    </a>
                    @else
                    <div class="certificado">
                        <img src="{{ asset('assets/img/certificados-e-homologacoes/'.$certificado->imagem) }}" alt="">
                        <span>{{ $certificado->nome }}</span>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>

        <div style="display:none">
            @foreach($certificados as $certificado)
                <div class="certificado-texto" id="certificado-{{ $certificado->id }}" style="display:none">
                    {!! $certificado->texto !!}
                </div>
            @endforeach
        </div>
    </div>

@endsection
