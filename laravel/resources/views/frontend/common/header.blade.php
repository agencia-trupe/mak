    <header @if(Route::currentRouteName() == 'home') class="home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
            <nav class="nav-desktop">
                @include('frontend.common.nav')
            </nav>
            <nav class="nav-secundaria">
                <span class="telefone">{{ $contato->telefone }}</span>
                <span class="whatsapp">{{ $contato->whatsapp }}</span>
                <div class="links">
                    <a href="{{ route('pesquisa') }}" @if(Tools::isActive('pesquisa')) class="active" @endif>pesquisa de satisfação</a>
                    <a href="{{ route('trabalhe') }}" @if(Tools::isActive('trabalhe')) class="active" @endif>trabalhe conosco</a>
                </div>
                @if($contato->facebook)
                    <a href="{{ $contato->facebook }}" class="facebook" target="_blank"></a>
                @endif
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </header>

    <nav class="nav-mobile" style="z-index:9997">
        @include('frontend.common.nav')
        <a href="{{ route('pesquisa') }}" @if(Tools::isActive('pesquisa')) class="active" @endif>pesquisa de satisfação</a>
        <a href="{{ route('trabalhe') }}" @if(Tools::isActive('trabalhe')) class="active" @endif>trabalhe conosco</a>
    </nav>
