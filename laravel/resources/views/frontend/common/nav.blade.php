<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>Home</a>
<a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>Empresa</a>
<a href="{{ route('produtos') }}" @if(Tools::isActive('produtos*')) class="active" @endif>Produtos</a>
<a href="{{ route('servicos') }}" @if(Tools::isActive('servicos')) class="active" @endif>Serviços</a>
<a href="{{ route('cases') }}" @if(Tools::isActive('cases*')) class="active" @endif>Cases e Obras</a>
<a href="{{ route('clientes') }}" @if(Tools::isActive('clientes*')) class="active" @endif>Clientes</a>
<a href="{{ route('tabelas') }}" @if(Tools::isActive('tabelas')) class="active" @endif>Tabelas</a>
<a href="{{ route('blog') }}" @if(Tools::isActive('blog*')) class="active" @endif>Blog</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>Contato</a>
