    <footer>
        <div class="center">
            <div class="col">
                <a href="{{ route('home') }}">HOME</a>
                <a href="{{ route('empresa') }}">EMPRESA</a>
                <a href="{{ route('cases') }}">CASES E OBRAS</a>
                <a href="{{ route('clientes') }}">CLIENTES</a>
                <a href="{{ route('tabelas') }}">TABELAS</a>
                <a href="{{ route('blog') }}">BLOG</a>
            </div>
            <div class="col">
                <a href="{{ route('produtos') }}">PRODUTOS</a>
                <div class="lista">
                    @foreach($produtosLinhasFooter as $linha)
                    <a href="{{ route('produtos', $linha->slug) }}">
                        {{ $linha->titulo }}
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <a href="{{ route('servicos') }}">SERVIÇOS</a>
                <div class="lista">
                    @foreach($servicosFooter as $servico)
                    <a href="{{ route('servicos', $servico->slug) }}">
                        {{ $servico->titulo }}
                    </a>
                    @endforeach
                </div>
            </div>
            <div class="col">
                <a href="{{ route('contato') }}">CONTATO</a>
                <p class="telefone">{{ $contato->telefone }}</p>
                @if($contato->telefone_suporte)
                    <p class="telefone">{{ $contato->telefone_suporte }} (suporte)</p>
                @endif
                <p class="whatsapp">{{ $contato->whatsapp }}</p>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>
        </div>
        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} Mak Painéis Elétricos Ltda - Todos os direitos reservados.
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
