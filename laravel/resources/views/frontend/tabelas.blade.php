@extends('frontend.common.template')

@section('content')

    <div class="main tabelas">
        <div class="center">
            <div class="apresentacao">
                <h2 class="titulo">TABELAS</h2>
                {!! $apresentacao->texto !!}
            </div>

            <div class="thumbs">
                @foreach($tabelas as $tabela)
                    <a href="{{ asset('assets/tabelas/'.$tabela->arquivo) }}" target="_blank">
                        {{ $tabela->titulo }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
