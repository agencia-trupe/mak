@extends('frontend.common.template')

@section('content')

    <div class="main pesquisa">
        <div class="center">
            <h2 class="titulo">PESQUISA DE SATISFAÇÃO</h2>

            @if(session('enviado'))
            <div class="enviado">
                Pesquisa enviada com sucesso!
            </div>
            @else
            <p>
                Prezado Cliente,<br>
                Estamos interessados em melhorar cada vez mais o atendimento que prestamos à sua Organização. Portanto, é fundamental sua opinião sobre nossas atividades, com intuito de melhoria em relação aos nossos produtos e serviços.
            </p>
            <form action="{{ route('pesquisa.post') }}" method="POST">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="erros">
                    @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                    @endforeach
                    </div>
                @endif

                <div class="dados">
                    <input type="text" name="empresa" id="empresa" placeholder="empresa" value="{{ old('empresa') }}" required>
                    <input type="text" name="contato" id="contato" placeholder="contato" value="{{ old('contato') }}" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                    <input type="text" name="departamento" id="departamento" placeholder="departamento" value="{{ old('departamento') }}" required>
                    <input type="text" name="pedido" id="pedido" placeholder="n° do pedido ou obra" value="{{ old('pedido') }}" required>
                </div>

                <div class="pesquisa">
                    <table cellspacing="0">
                        <thead>
                            <th style="width:99%"></th>
                            <th style="min-width:65px">ÓTIMO</th>
                            <th style="min-width:65px">BOM</th>
                            <th style="min-width:65px">RUIM</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>1.</strong> Atendimento comercial</td>
                                <td>
                                    <input type="radio" name="atendimento_comercial" value="Ótimo" required @if(old('atendimento_comercial') == 'Ótimo') checked @endif>
                                </td>
                                <td>
                                    <input type="radio" name="atendimento_comercial" value="Bom" required @if(old('atendimento_comercial') == 'Bom') checked @endif>
                                </td>
                                <td>
                                    <input type="radio" name="atendimento_comercial" value="Ruim" required @if(old('atendimento_comercial') == 'Ruim') checked @endif>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>2.</strong> Projetos e suporte técnico</td>
                                <td>
                                    <input type="radio" name="projetos_e_suporte_tecnico" value="Ótimo" required @if(old('projetos_e_suporte_tecnico') == 'Ótimo') checked @endif>
                                </td>
                                <td>
                                    <input type="radio" name="projetos_e_suporte_tecnico" value="Bom" required @if(old('projetos_e_suporte_tecnico') == 'Bom') checked @endif>
                                </td>
                                <td>
                                    <input type="radio" name="projetos_e_suporte_tecnico" value="Ruim" required @if(old('projetos_e_suporte_tecnico') == 'Ruim') checked @endif>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>3.</strong> Qualidade do produto</td>
                                <td>
                                    <input type="radio" name="qualidade_do_produto" value="Ótimo" required @if(old('qualidade_do_produto') == 'Ótimo') checked @endif>
                                </td>
                                <td>
                                    <input type="radio" name="qualidade_do_produto" value="Bom" required @if(old('qualidade_do_produto') == 'Bom') checked @endif>
                                </td>
                                <td>
                                    <input type="radio" name="qualidade_do_produto" value="Ruim" required @if(old('qualidade_do_produto') == 'Ruim') checked @endif>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>4.</strong> Pós-venda (dúvidas ou reclamações)</td>
                                <td>
                                    <input type="radio" name="pos_venda" value="Ótimo" required @if(old('pos_venda') == 'Ótimo') checked @endif>
                                </td>
                                <td>
                                    <input type="radio" name="pos_venda" value="Bom" required @if(old('pos_venda') == 'Bom') checked @endif>
                                </td>
                                <td>
                                    <input type="radio" name="pos_venda" value="Ruim" required @if(old('pos_venda') == 'Ruim') checked @endif>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <strong>5.</strong> Indicaria a Mak Painéis?
                                    <label>
                                        <input type="radio" name="indicaria" value="Sim" required @if(old('indicaria') == 'Sim') checked @endif>
                                        SIM
                                    </label>
                                    <label>
                                        <input type="radio" name="indicaria" value="Não" required @if(old('indicaria') == 'Não') checked @endif>
                                        NÃO
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <textarea name="mensagem" placeholder="6. Este campo é para suas sugestões, críticas ou melhoria para os nossos produtos e serviços: (não há necessidade de preenchimento, ficando a disposição)">{{ old('mensagem') }}</textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <input type="submit" value="ENVIAR">

                    <div class="agradecimento">
                        <p>Agradecemos o seu interesse. Sua opinião é muito valiosa.</p>
                        <p>Atenciosamente,<br>Mak Painéis Elétricos Ltda</p>
                    </div>
                </div>
            </form>
            @endif
        </div>
    </div>

@endsection
