@extends('frontend.common.template')

@section('content')

    <div class="main cases">
        <div class="center">
            <h2 class="titulo">CASES E OBRAS</h2>

            <div class="thumbs">
                @foreach($cases as $case)
                <a href="{{ route('cases', $case->slug) }}">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/cases-e-obras/'.$case->capa) }}" alt="">
                        <div class="overlay"></div>
                    </div>
                    <span>{{ $case->titulo }}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
