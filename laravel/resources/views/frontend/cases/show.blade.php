@extends('frontend.common.template')

@section('content')

    <?php
        $imagensMetade = ceil($case->imagens->count() / 2);
        $imagensChunks = $case->imagens->chunk($imagensMetade);
    ?>

    <div class="main cases">
        <div class="center">
            <h2 class="titulo">CASES E OBRAS</h2>

            <div class="left">
                <h1>{{ $case->titulo }}</h1>
                {!! $case->descricao !!}

                @foreach($imagensChunks[0] as $imagem)
                    <img src="{{ asset('assets/img/cases-e-obras/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>

            <div class="right">
                @foreach($imagensChunks[1] as $imagem)
                    <img src="{{ asset('assets/img/cases-e-obras/imagens/'.$imagem->imagem) }}" alt="">
                @endforeach
            </div>
        </div>
    </div>

@endsection
