@extends('frontend.common.template')

@section('content')

    <div class="main clientes">
        <div class="center">
            <h2 class="titulo">CLIENTES</h2>

            <div class="marcas">
                @foreach($categoria->clientes as $cliente)
                    <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}">
                @endforeach
            </div>

            <nav>
                @foreach($categorias as $c)
                    <a href="{{ route('clientes', $c->slug) }}" @if($categoria->slug === $c->slug) class="active" @endif>{{ $c->titulo }}</a>
                @endforeach
            </nav>
        </div>
    </div>

@endsection
