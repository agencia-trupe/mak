@extends('frontend.common.template')

@section('content')

    <div class="main servicos">
        <div class="center">
            <h2 class="titulo">SERVIÇOS</h2>

            <div class="servico">
                <div class="wrapper">
                    <img src="{{ asset('assets/img/servicos/'.$servico->imagem) }}" alt="">
                    <div class="texto">
                        <h2>{{ $servico->titulo }}</h2>
                        {!! $servico->descricao !!}
                    </div>
                </div>
            </div>

            <nav>
                @foreach($servicos as $s)
                    <a href="{{ route('servicos', $s->slug) }}" @if($servico->slug === $s->slug) class="active" @endif>{{ $s->titulo }}</a>
                @endforeach
            </nav>
        </div>
    </div>

@endsection
