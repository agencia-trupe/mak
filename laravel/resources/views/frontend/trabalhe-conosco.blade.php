@extends('frontend.common.template')

@section('content')

    <div class="main trabalhe">
        <div class="center">
            <h2 class="titulo">TRABALHE CONOSCO</h2>

            <img src="{{ asset('assets/img/layout/trabalhe.jpg') }}" alt="">

            @if(session('enviado'))
            <div class="enviado">
                Currículo enviado com sucesso!
            </div>
            @else
            <form action="{{ route('trabalhe.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}

                @if($errors->any())
                    <div class="erros">
                    @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                    @endforeach
                    </div>
                @endif

                <div class="inputs">
                    <input type="text" name="nome" id="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                    <input type="text" name="cargo" id="cargo" placeholder="área de atuação | cargo pretendido" value="{{ old('cargo') }}" required>
                </div>

                <div class="btn-curriculo">
                    <input type="file" name="curriculo" id="input-curriculo" required>
                    <span>ANEXAR CURRÍCULO</span>
                </div>

                <input type="submit" value="ENVIAR">
            </form>
            @endif
        </div>
    </div>

@endsection
