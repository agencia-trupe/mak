@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <h2 class="titulo">CONTATO</h2>

            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                @if($contato->telefone_suporte)
                    <p class="telefone">{{ $contato->telefone_suporte }} (suporte)</p>
                @endif
                <p class="whatsapp">{{ $contato->whatsapp }}</p>
                <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                <p class="endereco">{!! $contato->endereco !!}</p>
            </div>

            <form action="" id="form-contato" method="POST">
                <h2>FALE CONOSCO</h2>
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <select name="assunto" id="assunto" required>
                    <option value="" selected disabled>assunto [SELECIONE...]</option>
                    <option value="informações">informações</option>
                    <option value="dúvidas">dúvidas</option>
                    <option value="reclamações">reclamações</option>
                </select>
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <div id="form-contato-response"></div>
                <input type="submit" value="ENVIAR">
            </form>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
