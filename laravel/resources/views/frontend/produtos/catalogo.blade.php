<div class="popup-catalogo">
    <h2>
        <span>DOWNLOAD</span>
        {{ $catalogo->nome }}
    </h2>
    <form action="" id="form-catalogo" method="POST">
        <input type="hidden" name="id" id="id" value="{{ $catalogo->id }}">
        <input type="text" name="nome" id="nome" placeholder="nome" required>
        <input type="email" name="email" id="email" placeholder="e-mail" required>
        <input type="text" name="telefone" id="telefone" placeholder="telefone" required>
        <div id="form-catalogo-response"></div>
        <input type="submit" value="ENVIAR">
    </form>
    <div class="catalogo-download-link">
        <a href="#" target="_blank">FAZER DOWNLOAD</a>
    </div>
</div>
