@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <div class="thumbs">
                <div class="wrapper">
                    <div class="aplicacao-img">
                        <img src="{{ asset('assets/img/aplicacoes/'.$aplicacao->imagem) }}" alt="">
                    </div>
                </div>
            </div>

            <nav>
                <h2 class="titulo">LINHA</h2>
                @foreach($linhas as $l)
                <a href="{{ route('produtos', $l->slug) }}" @if(isset($linha) && $l->slug == $linha->slug) class="active" @endif>{{ $l->titulo }}</a>
                @endforeach

                @foreach($catalogos as $catalogo)
                <a href="{{ route('catalogo', $catalogo->id) }}" target="_blank" class="catalogo catalogo-popup">
                    <span>DOWNLOAD</span>
                    {{ $catalogo->nome }}
                </a>
                @endforeach

                <a href="{{ route('produtos.aplicacoes') }}" class="btn-aplicacoes active">
                    <span>APLICAÇÕES POR SEGMENTO</span>
                    Clique para ver exemplos de arquitetura por segmento
                </a>
            </nav>

            <div class="voltar-produto">
                <a href="{{ route('produtos.aplicacoes') }}">
                    voltar
                </a>
            </div>
        </div>
    </div>

@endsection
