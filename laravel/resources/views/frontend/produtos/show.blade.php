@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <div class="show">
                <div class="wrapper">
                    <div>
                        <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                        <div class="texto">
                            <h2>{{ $produto->titulo }}</h2>
                            {!! $produto->resumo !!}
                        </div>
                        <div class="descricao">
                            {!! $produto->descricao !!}
                        </div>
                        <div class="caracteristicas">
                            <h5>CARACTERÍSTICAS</h5>
                            @if($produto->corrente_nominal_do_barramento_principal)
                            <div>
                                <span>Corrente Nominal do Barramento Principal</span>
                                <span>{{ $produto->corrente_nominal_do_barramento_principal }}</span>
                            </div>
                            @endif
                            @if($produto->corrente_suportavel_de_curta_duracao)
                            <div>
                                <span>Corrente Suportável de Curta Duração</span>
                                <span>{{ $produto->corrente_suportavel_de_curta_duracao }}</span>
                            </div>
                            @endif
                            @if($produto->corrente_suportavel_de_crista)
                            <div>
                                <span>Corrente Suportável de Crista</span>
                                <span>{{ $produto->corrente_suportavel_de_crista }}</span>
                            </div>
                            @endif
                            @if($produto->grau_de_protecao)
                            <div>
                                <span>Grau de Proteção</span>
                                <span>{{ $produto->grau_de_protecao }}</span>
                            </div>
                            @endif
                            @if($produto->cor_padrao)
                            <div>
                                <span>Cor Padrão</span>
                                <span>{{ $produto->cor_padrao }}</span>
                            </div>
                            @endif
                            @if($produto->tipo_de_construcao)
                            <div>
                                <span>Tipo de construção</span>
                                <span>{{ $produto->tipo_de_construcao }}</span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <nav>
                <h2 class="titulo">LINHA</h2>
                @foreach($linhas as $l)
                <a href="{{ route('produtos', $l->slug) }}" @if(isset($linha) && $l->slug == $linha->slug) class="active" @endif>{{ $l->titulo }}</a>
                @endforeach

                @foreach($catalogos as $catalogo)
                <a href="{{ route('catalogo', $catalogo->id) }}" target="_blank" class="catalogo catalogo-popup">
                    <span>DOWNLOAD</span>
                    {{ $catalogo->nome }}
                </a>
                @endforeach

                <a href="{{ route('produtos.aplicacoes') }}" class="btn-aplicacoes">
                    <span>APLICAÇÕES POR SEGMENTO</span>
                    Clique para ver exemplos de arquitetura por segmento
                </a>
            </nav>

            <div class="voltar-produto">
                <a href="{{ route('produtos', $produto->linha->slug) }}">
                    voltar
                </a>
            </div>
        </div>
    </div>

@endsection
