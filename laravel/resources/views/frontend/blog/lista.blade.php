@extends('frontend.common.template')

@section('content')

    <div class="main blog">
        <div class="center">
            <h2 class="titulo">BLOG</h2>

            <div class="lista">
                <div class="wrapper">
                    @foreach($posts as $post)
                    <a href="{{ route('blog', $post->slug) }}">
                        {{ $post->titulo }}
                        <span>{{ Tools::formataData($post->data) }}</span>
                    </a>
                    @endforeach
                </div>
            </div>

            <nav>
                <h2 class="titulo">CATEGORIAS</h2>
                @foreach($categorias as $c)
                <a href="{{ route('blog.categoria', $c->slug) }}" @if(isset($categoria) && $c->slug == $categoria->slug) class="active" @endif>
                    {{ $c->titulo }}
                </a>
                @endforeach

                <h2 class="titulo">ARQUIVO</h2>
                @foreach($arquivos as $a)
                <a href="{{ route('blog.arquivo', [$a->ano, $a->mes]) }}" @if(isset($ano) && isset($mes) && ($a->ano == $ano) && ($a->mes == $mes)) class="active" @endif>
                    {{ Tools::formataArquivo($a->ano, $a->mes) }}
                </a>
                @endforeach
            </nav>
        </div>
    </div>

@endsection
