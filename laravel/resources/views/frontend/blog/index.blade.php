@extends('frontend.common.template')

@section('content')

    <div class="main blog">
        <div class="center">
            <h2 class="titulo">BLOG</h2>

            <div class="post">
                <div class="conteudo">
                    <div class="wrapper">
                        <h1>
                            {{ $post->titulo }}
                            <span>{{ Tools::formataData($post->data) }}</span>
                        </h1>

                        {!! $post->texto !!}
                    </div>
                </div>

                <div class="comentario">
                    <form action="" id="form-comentario">
                        <h5>Comente!</h5>
                        <input type="hidden" name="post" id="post" value="{{ $post->id }}">
                        <input type="text" name="nome" id="nome" placeholder="nome" required>
                        <input type="email" name="email" id="email" placeholder="e-mail" required>
                        <textarea name="comentario" id="comentario" placeholder="comentário" required></textarea>
                        <input type="submit" value="&raquo;">
                        <div id="form-comentario-response"></div>
                    </form>

                    @foreach($post->comentariosAprovados as $comentario)
                    <div class="comentario">
                        <span class="dados">{{ $comentario->nome }} &middot; {{ Tools::formataData($comentario->created_at) }}</span>
                        <p>{{ $comentario->comentario }}</p>
                    </div>
                    @endforeach
                </div>

                <div class="navegacao">
                    @if($anterior)
                    <a href="{{ route('blog', $anterior) }}" class="anterior">
                        &laquo; post anterior
                    </a>
                    @endif
                    @if($proximo)
                    <a href="{{ route('blog', $proximo) }}" class="proximo">
                        próximo post &raquo;
                    </a>
                    @endif
                </div>
            </div>

            <nav>
                <h2 class="titulo">CATEGORIAS</h2>
                @foreach($categorias as $c)
                <a href="{{ route('blog.categoria', $c->slug) }}" @if(isset($categoria) && $c->slug == $categoria->slug) class="active" @endif>
                    {{ $c->titulo }}
                </a>
                @endforeach

                <h2 class="titulo">ARQUIVO</h2>
                @foreach($arquivos as $a)
                <a href="{{ route('blog.arquivo', [$a->ano, $a->mes]) }}" @if(isset($ano) && isset($mes) && ($a->ano == $ano) && ($a->mes == $mes)) class="active" @endif>
                    {{ Tools::formataArquivo($a->ano, $a->mes) }}
                </a>
                @endforeach
            </nav>
        </div>
    </div>

@endsection
