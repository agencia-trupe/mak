@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner">
            <div class="center">
                <div class="banner-texto">
                    <div>
                        <h2>{{ $banner->titulo }}</h2>
                        <p>{!! $banner->descricao !!}</p>
                        <a href="{{ $banner->link }}">saber mais &raquo;</a>
                    </div>
                </div>
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
            </div>
        </div>
        @endforeach
        <div class="cycle-pager" style="z-index:101"></div>
    </div>

    <div class="home-produtos">
        <div class="center">
            @foreach($produtos as $produto)
            <a href="{{ route('produtos.show', [$produto->linha->slug, $produto->slug]) }}">
                <img src="{{ asset('assets/img/produtos/destaque/'.$produto->imagem) }}" alt="">
                <span>{{ $produto->titulo }}</span>
            </a>
            @endforeach
        </div>
    </div>

    <div class="home-chamadas">
        <div class="center">
            <div class="chamadas">
                <a href="{{ $chamadas->link_1 }}" class="chamada">
                    <h4>{{ $chamadas->titulo_1 }}</h4>
                    <img src="{{ asset('assets/img/chamadas/'.$chamadas->imagem_1) }}" alt="">
                    <p>{{ $chamadas->texto_1 }}</p>
                    <span class="seta"></span>
                </a>
                <a href="{{ $chamadas->link_2 }}" class="chamada">
                    <h4>{{ $chamadas->titulo_2 }}</h4>
                    <img src="{{ asset('assets/img/chamadas/'.$chamadas->imagem_2) }}" alt="">
                    <p>{{ $chamadas->texto_2 }}</p>
                    <span class="seta"></span>
                </a>
            </div>
            <div class="novidades">
                @foreach($novidades as $novidade)
                <a href="{{ route('blog', $novidade->slug) }}">
                    <h5>NOVIDADES</h5>
                    <p>{{ $novidade->titulo }}</p>
                </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="home-financiamentos">
        <div class="center">
            <h3>FINANCIAMENTOS</h3>

            @foreach($financiamentos as $financiamento)
            <div class="financiamento">
                <h5>{{ $financiamento->nome }}</h5>
                <img src="{{ asset('assets/img/financiamentos/'.$financiamento->imagem) }}" alt="">
            </div>
            @endforeach
        </div>
    </div>

@endsection
