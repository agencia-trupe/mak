<!DOCTYPE html>
<html>
<head>
    <title>[COMENTÁRIO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Post:</span> <a href="{{ route('blog', $post->slug) }}" style='color:#000;font-size:14px;font-family:Verdana;'>{{ $post->titulo }}</a><br><br>

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $comentario['nome'] }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $comentario['email'] }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Comentário:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $comentario['comentario'] }}</span><br><br>

    <span style='font-weight:normal;font-size:14px;font-family:Verdana;'>Acesse o painel de administração para aprovar o comentário.</span><br>
</body>
</html>
