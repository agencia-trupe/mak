<!DOCTYPE html>
<html>
<head>
    <title>[PESQUISA DE SATISFAÇÃO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Contato:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $contato }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
@if($telefone)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Departamento:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $departamento }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Número do pedido ou obra:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $pedido }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Pesquisa:</span><br>
    <div style='color:#000;font-size:14px;font-family:Verdana;'>
        <strong>1.</strong> Atendimento comercial: {{ $atendimento_comercial }}<br>
        <strong>2.</strong> Projetos e suporte técnico: {{ $projetos_e_suporte_tecnico }}<br>
        <strong>3.</strong> Qualidade do produto: {{ $qualidade_do_produto }}<br>
        <strong>4.</strong> Pós-venda (dúvidas ou reclamações): {{ $pos_venda }}<br>
        <strong>5.</strong> Indicaria a Mak Painéis: {{ $indicaria }}
        @if($mensagem)
        <br>
        <strong>6.</strong> Mensagem: {{ $mensagem }}
        @endif
    </div>
</body>
</html>
