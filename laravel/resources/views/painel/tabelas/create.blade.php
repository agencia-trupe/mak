@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tabelas /</small> Adicionar Tabela</h2>
    </legend>

    {!! Form::open(['route' => 'painel.tabelas.store', 'files' => true]) !!}

        @include('painel.tabelas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
