@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tabelas /</small> Editar Tabela</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.tabelas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.tabelas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
