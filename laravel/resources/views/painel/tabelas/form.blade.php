@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if(isset($registro) && $registro->arquivo)
    <p>
        <a href="{{ url('assets/tabelas/'.$registro->arquivo) }}" target="_blank" style="display:block;">{{ $registro->arquivo }}</a>
    </p>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.tabelas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
