@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Catálogo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.catalogo.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos-catalogo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
