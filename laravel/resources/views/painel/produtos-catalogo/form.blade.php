@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if($registro->arquivo)
    <p>
        <a href="{{ url('assets/produtos/catalogo/'.$registro->arquivo) }}" target="_blank" style="display:block;">{{ $registro->arquivo }}</a>
    </p>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
