<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <title>{{ config('site.name') }} - Painel Administrativo</title>

    {!! Tools::loadCss('vendor/bootswatch-dist/css/bootstrap.min.css') !!}
    {!! Tools::loadCss('vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') !!}
    {!! Tools::loadCss('css/vendor.painel.css') !!}
    {!! Tools::loadCss('css/painel.css') !!}
</head>
<body>
    @include('painel.common.header')

    <div class="container" style="padding-bottom:30px;">
        @yield('content')
    </div>

    {!! Tools::loadJquery() !!}
    {!! Tools::loadJs('vendor/ckeditor/ckeditor.js') !!}
    {!! Tools::loadJs('js/vendor.painel.js') !!}
    {!! Tools::loadJs('js/painel.js') !!}
</body>
</html>
