<ul class="nav navbar-nav">
    <li class="dropdown @if(str_is('painel.banners*', Route::currentRouteName()) || str_is('painel.chamadas*', Route::currentRouteName()) || str_is('painel.financiamentos*', Route::currentRouteName()) || str_is('painel.aviso-home*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.aviso-home*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aviso-home.index') }}">Aviso Home</a>
            </li>
            <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(str_is('painel.chamadas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
            </li>
            <li @if(str_is('painel.financiamentos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.financiamentos.index') }}">Financiamentos</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.empresa*', Route::currentRouteName()) || str_is('painel.evolucao-dos-catalogos*', Route::currentRouteName()) || str_is('painel.evolucao-da-marca*', Route::currentRouteName()) || str_is('painel.certificados-e-homologacoes*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Empresa
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.empresa.index') }}">Empresa</a>
            </li>
            <li @if(str_is('painel.evolucao-da-marca*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.evolucao-da-marca.index') }}">Evolução da Marca</a>
            </li>
            <li @if(str_is('painel.evolucao-dos-catalogos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.evolucao-dos-catalogos.index') }}">Evolução dos Catálogos</a>
            </li>
            <li @if(str_is('painel.certificados-e-homologacoes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.certificados-e-homologacoes.index') }}">Certificados e Homologações</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.produtos.index') }}">Produtos</a>
    </li>
	<li @if(str_is('painel.aplicacoes*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.aplicacoes.index') }}">Aplicações</a>
	</li>
    <li @if(str_is('painel.servicos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.servicos.index') }}">Serviços</a>
    </li>
	<li @if(str_is('painel.cases-e-obras*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.cases-e-obras.index') }}">Cases e Obras</a>
	</li>
    <li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.clientes.index') }}">Clientes</a>
    </li>
	<li @if(str_is('painel.tabelas*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.tabelas.index') }}">Tabelas</a>
	</li>
    <li @if(str_is('painel.blog*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.blog.index') }}">Blog</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($totalNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $totalNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li><a href="{{ route('painel.contato.trabalhe-conosco.index') }}">
                Trabalhe Conosco
                @if($trabalheNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $trabalheNaoLidos }}</span>
                @endif
            </a></li>
            <li><a href="{{ route('painel.contato.pesquisa-de-satisfacao.index') }}">
                Pesquisa de Satisfação
                @if($pesquisaNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $pesquisaNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
