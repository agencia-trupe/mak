@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cases e Obras /</small> Adicionar Case / Obra</h2>
    </legend>

    {!! Form::open(['route' => 'painel.cases-e-obras.store', 'files' => true]) !!}

        @include('painel.cases-e-obras.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
