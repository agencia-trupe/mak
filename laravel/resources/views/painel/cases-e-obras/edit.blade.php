@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cases e Obras /</small> Editar Case / Obra</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cases-e-obras.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cases-e-obras.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
