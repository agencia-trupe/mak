@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'empresa']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao', 'Missão') !!}
            {!! Form::textarea('missao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('visao', 'Visão') !!}
            {!! Form::textarea('visao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('valores', 'Valores') !!}
            {!! Form::textarea('valores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            <img src="{{ url('assets/img/empresa/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            <img src="{{ url('assets/img/empresa/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            <img src="{{ url('assets/img/empresa/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
