@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Evolução dos Catálogos /</small> Editar Catálogo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.evolucao-dos-catalogos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.evolucao-dos-catalogos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
