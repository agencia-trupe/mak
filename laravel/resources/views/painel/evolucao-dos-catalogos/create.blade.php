@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Evolução dos Catálogos /</small> Adicionar Catálogo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.evolucao-dos-catalogos.store', 'files' => true]) !!}

        @include('painel.evolucao-dos-catalogos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
