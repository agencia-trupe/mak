@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tabelas /</small> Apresentação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.tabelas-apresentacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.tabelas-apresentacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
