@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Financiamentos /</small> Editar Financiamento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.financiamentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.financiamentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
