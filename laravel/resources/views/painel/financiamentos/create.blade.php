@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Financiamentos /</small> Adicionar Financiamento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.financiamentos.store', 'files' => true]) !!}

        @include('painel.financiamentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
