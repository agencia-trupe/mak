@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Evolução da Marca /</small> Adicionar Marca</h2>
    </legend>

    {!! Form::open(['route' => 'painel.evolucao-da-marca.store', 'files' => true]) !!}

        @include('painel.evolucao-da-marca.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
