@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Evolução da Marca /</small> Editar Marca</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.evolucao-da-marca.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.evolucao-da-marca.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
