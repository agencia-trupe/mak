@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('ano', 'Ano') !!}
    {!! Form::text('ano', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/evolucao-da-marca/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.evolucao-da-marca.index') }}" class="btn btn-default btn-voltar">Voltar</a>
