@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_1', 'Título 1') !!}
            {!! Form::text('titulo_1', null, ['class' => 'form-control']) !!}
        </div>

        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_1', 'Texto 1') !!}
            {!! Form::text('texto_1', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_1', 'Link 1') !!}
            {!! Form::text('link_1', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_2', 'Título 2') !!}
            {!! Form::text('titulo_2', null, ['class' => 'form-control']) !!}
        </div>

        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_2', 'Texto 2') !!}
            {!! Form::text('texto_2', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_2', 'Link 2') !!}
            {!! Form::text('link_2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
