@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Adicionar Produto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos.store', 'files' => true]) !!}

        @include('painel.produtos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
