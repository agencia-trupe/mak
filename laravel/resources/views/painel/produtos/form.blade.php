@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produtos_linha_id', 'Linha') !!}
    {!! Form::select('produtos_linha_id', $linhas, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('resumo', 'Resumo') !!}
    {!! Form::textarea('resumo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'produtoResumo']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('corrente_nominal_do_barramento_principal', 'Corrente Nominal do Barramento Principal ') !!}
    {!! Form::text('corrente_nominal_do_barramento_principal', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('corrente_suportavel_de_curta_duracao', 'Corrente Suportável de Curta Duração') !!}
    {!! Form::text('corrente_suportavel_de_curta_duracao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('corrente_suportavel_de_crista', 'Corrente Suportável de Crista') !!}
    {!! Form::text('corrente_suportavel_de_crista', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('grau_de_protecao', 'Grau de Proteção') !!}
    {!! Form::text('grau_de_protecao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cor_padrao', 'Cor Padrão') !!}
    {!! Form::text('cor_padrao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('tipo_de_construcao', 'Tipo de Construção') !!}
    {!! Form::text('tipo_de_construcao', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    <div class="checkbox">
        <label>
            <input type="hidden" name="destaque" value="0">
            {!! Form::checkbox('destaque', 1) !!} <strong>Marcar como Destaque na Home</strong>
        </label>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
