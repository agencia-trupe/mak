@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Segmento</h2>
    </legend>

    {!! Form::model($segmento, [
        'route'  => ['painel.produtos.segmentos.update', $segmento->id],
        'method' => 'patch'])
    !!}

    @include('painel.produtos.segmentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
