@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Adicionar Segmento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos.segmentos.store']) !!}

        @include('painel.produtos.segmentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
