@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.produtos.index') }}" title="Voltar para Produtos" class="btn btn-sm btn-default">
        &larr; Voltar para Produtos    </a>

    <legend>
        <h2>
            <small>Produtos /</small> Segmentos

            <a href="{{ route('painel.produtos.segmentos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Segmento</a>
        </h2>
    </legend>

    @if(!count($segmentos))
    <div class="alert alert-warning" role="alert">Nenhuma segmento cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="produtos_segmentos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($segmentos as $segmento)
            <tr class="tr-row" id="{{ $segmento->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $segmento->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.produtos.segmentos.destroy', $segmento->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.segmentos.edit', $segmento->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
