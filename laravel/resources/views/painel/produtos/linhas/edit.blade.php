@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Editar Linha</h2>
    </legend>

    {!! Form::model($linha, [
        'route'  => ['painel.produtos.linhas.update', $linha->id],
        'method' => 'patch'])
    !!}

    @include('painel.produtos.linhas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
