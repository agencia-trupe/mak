@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Blog / Categorias /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.blog.categorias.store', 'files' => true]) !!}

        @include('painel.blog.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
