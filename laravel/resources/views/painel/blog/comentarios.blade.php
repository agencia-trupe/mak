@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.blog.index') }}" class="btn btn-default btn-sm">
        &larr; Voltar para o Blog
    </a>

    <legend>
        <h2>
            <small>Blog / {{ $post->titulo }} /</small> Comentários<br>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="blog_posts">
        <thead>
            <tr>
                <th>Data</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Comentário</th>
                <th>Status</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row {{ $registro->aprovado ? 'success' : '' }}" id="{{ $registro->id }}">
                <td>{{ $registro->created_at }}</td>
                <td>{{ $registro->nome }}</td>
                <td>{{ $registro->email }}</td>
                <td>{{ $registro->comentario }}</td>
                <td>
                    @if($registro->aprovado)
                    <span class="glyphicon glyphicon-ok"></span>
                    @else
                    <span class="text-danger glyphicon glyphicon-remove"></span>
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.blog.comentarios.destroy', $post->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.blog.comentarios.aprovacao', [$post->id, $registro->id ]) }}" class="btn btn-primary btn-sm pull-left">
                            @if(!$registro->aprovado)
                            <span class="glyphicon glyphicon-ok" style="margin-right:10px;"></span>Aprovar
                            @else
                            <span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Recusar
                            @endif
                        </a>
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    @endif

@endsection
