@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Blog
            <div class="btn-group pull-right">
                <a href="{{ route('painel.blog.categorias.index') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Editar Categorias</a>
                <a href="{{ route('painel.blog.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Post</a>
            </div>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="blog_posts">
        <thead>
            <tr>
                <th>Data</th>
                <th>Categoria</th>
                <th>Título</th>
                <th>Comentários</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->data }}</td>
                <td>{!! $registro->categoria ? $registro->categoria->titulo : '<span class="label label-danger">sem categoria</span>' !!}</td>
                <td>{{ $registro->titulo }}</td>
                <td><a href="{{ route('painel.blog.comentarios.index', $registro->id) }}" class="btn btn-warning btn-sm">
                    <span class="glyphicon glyphicon-comment" style="margin-right:10px;"></span>Gerenciar
                    @if($registro->comentariosParaAprovar >= 1)
                    <span class="badge" style="margin-left:6px;">{{ $registro->comentariosParaAprovar }}</span>
                    @endif
                </a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.blog.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.blog.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->links() !!}

    @endif

@endsection
