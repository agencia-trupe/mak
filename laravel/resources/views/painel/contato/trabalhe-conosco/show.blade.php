@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Trabalhe Conosco</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $contato->email }}
        </div>
    </div>

@if($contato->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $contato->telefone }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Área de Atuação / Cargo</label>
        <div class="well">{{ $contato->cargo }}</div>
    </div>

    <div class="form-group">
        <label>Currículo</label>
        <div class="well">
            <a href="{{ asset('curriculos/'.$contato->curriculo) }}" target="_blank">{{ $contato->curriculo }}</a>
        </div>
    </div>

    <a href="{{ route('painel.contato.trabalhe-conosco.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
