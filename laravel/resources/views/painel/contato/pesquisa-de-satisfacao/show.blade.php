@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Pesquisa de Satisfação</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $contato->empresa }}</div>
    </div>

    <div class="form-group">
        <label>Contato</label>
        <div class="well">{{ $contato->contato }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $contato->email }}
        </div>
    </div>

@if($contato->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $contato->telefone }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Departamento</label>
        <div class="well">{{ $contato->departamento }}</div>
    </div>

    <div class="form-group">
        <label>Número do pedido ou obra</label>
        <div class="well">{{ $contato->pedido }}</div>
    </div>

    <div class="form-group">
        <label>Pesquisa</label>
        <div class="well">
            <strong>1.</strong> Atendimento comercial: {{ $contato->atendimento_comercial }}<br>
            <strong>2.</strong> Projetos e suporte técnico: {{ $contato->projetos_e_suporte_tecnico }}<br>
            <strong>3.</strong> Qualidade do produto: {{ $contato->qualidade_do_produto }}<br>
            <strong>4.</strong> Pós-venda (dúvidas ou reclamações): {{ $contato->pos_venda }}<br>
            <strong>5.</strong> Indicaria a Mak Painéis: {{ $contato->indicaria }}
            @if($contato->mensagem)
            <br><strong>6.</strong> Mensagem: {{ $contato->mensagem }}
            @endif
        </div>
    </div>

    <a href="{{ route('painel.contato.trabalhe-conosco.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
