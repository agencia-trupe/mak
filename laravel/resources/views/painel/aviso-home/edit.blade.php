@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Aviso Home</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aviso-home.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aviso-home.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
