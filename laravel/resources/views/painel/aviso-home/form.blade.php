@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'aviso']) !!}
</div>

<div class="form-group">
    <div class="checkbox">
        <label>
            <input type="hidden" name="ativo" value="0">
            {!! Form::checkbox('ativo', 1) !!} <strong>Ativo</strong>
        </label>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
