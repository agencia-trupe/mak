@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Certificados e Homologações /</small> Adicionar Certificado</h2>
    </legend>

    {!! Form::open(['route' => 'painel.certificados-e-homologacoes.store', 'files' => true]) !!}

        @include('painel.certificados-e-homologacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
