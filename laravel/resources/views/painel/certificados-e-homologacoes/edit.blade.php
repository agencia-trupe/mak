@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Certificados e Homologações /</small> Editar Certificado</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.certificados-e-homologacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.certificados-e-homologacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
