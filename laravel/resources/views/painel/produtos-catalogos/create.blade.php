@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Catálogos /</small> Adicionar Catálogo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.produtos.catalogos.store', 'files' => true]) !!}

        @include('painel.produtos-catalogos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
