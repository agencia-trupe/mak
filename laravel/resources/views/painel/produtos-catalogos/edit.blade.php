@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Catálogos /</small> Editar Catálogo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.catalogos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos-catalogos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
