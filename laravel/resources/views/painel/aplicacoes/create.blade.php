@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aplicações /</small> Adicionar Aplicação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aplicacoes.store', 'files' => true]) !!}

        @include('painel.aplicacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
