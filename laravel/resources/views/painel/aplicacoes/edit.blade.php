@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aplicações /</small> Editar Aplicação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aplicacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aplicacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
