@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Clientes /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.clientes.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.clientes.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
