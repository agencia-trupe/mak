@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Clientes /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.clientes.categorias.store']) !!}

        @include('painel.clientes.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
