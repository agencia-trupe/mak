<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('aplicacoes', 'App\Models\Aplicacao');
		$router->model('certificados-e-homologacoes', 'App\Models\Certificado');
		$router->model('aviso-home', 'App\Models\AvisoHome');
		$router->model('catalogos', 'App\Models\ProdutoCatalogo');
		$router->model('cases-e-obras', 'App\Models\CaseObra');
		$router->model('imagens_cases-e-obras', 'App\Models\CaseObraImagem');
		$router->model('catalogo', 'App\Models\ProdutosCatalogo');
		$router->model('produtos', 'App\Models\Produto');
		$router->model('linhas_produtos', 'App\Models\ProdutoLinha');
		$router->model('segmentos_produtos', 'App\Models\ProdutoSegmento');
        $router->model('comentarios', 'App\Models\BlogComentario');
        $router->model('blog', 'App\Models\BlogPost');
        $router->model('categorias', 'App\Models\BlogCategoria');
		$router->model('tabelas-apresentacao', 'App\Models\TabelasApresentacao');
		$router->model('tabelas', 'App\Models\Tabela');
		$router->model('clientes', 'App\Models\Cliente');
		$router->model('categorias_clientes', 'App\Models\ClienteCategoria');
		$router->model('servicos', 'App\Models\Servico');
		$router->model('evolucao-dos-catalogos', 'App\Models\Catalogo');
		$router->model('evolucao-da-marca', 'App\Models\Marca');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('chamadas', 'App\Models\Chamadas');
		$router->model('financiamentos', 'App\Models\Financiamento');
		$router->model('banners', 'App\Models\Banner');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('trabalhe_conosco', 'App\Models\TrabalheConosco');
        $router->model('pesquisa_de_satisfacao', 'App\Models\Pesquisa');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('servico_slug', function($value) {
            return \App\Models\Servico::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('case_slug', function($value) {
            return \App\Models\CaseObra::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('clientes_categoria', function($value) {
            return \App\Models\ClienteCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('produtos_linha', function($value) {
            return \App\Models\ProdutoLinha::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('produto_slug', function($value) {
            return \App\Models\Produto::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('produtos_segmento', function($value) {
            return \App\Models\ProdutoSegmento::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('blog_categoria', function($value) {
            return \App\Models\BlogCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('post_slug', function($value) {
            return \App\Models\BlogPost::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
