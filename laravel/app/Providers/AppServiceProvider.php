<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $contatosNaoLidos = \App\Models\ContatoRecebido::naoLidos()->count();
            $trabalheNaoLidos = \App\Models\TrabalheConosco::naoLidos()->count();
            $pesquisaNaoLidos = \App\Models\Pesquisa::naoLidos()->count();

            $totalNaoLidos = $contatosNaoLidos + $trabalheNaoLidos + $pesquisaNaoLidos;

            $view->with('contatosNaoLidos', $contatosNaoLidos);
            $view->with('trabalheNaoLidos', $trabalheNaoLidos);
            $view->with('pesquisaNaoLidos', $pesquisaNaoLidos);
            $view->with('totalNaoLidos', $totalNaoLidos);
        });
        view()->composer('frontend.common.*', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });
        view()->composer('frontend.common.footer', function($view) {
            $view->with('produtosLinhasFooter', \App\Models\ProdutoLinha::ordenados()->get());
            $view->with('servicosFooter', \App\Models\Servico::ordenados()->get(['titulo', 'slug']));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
