<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AvisoHome extends Model
{
    protected $table = 'aviso_home';

    protected $guarded = ['id'];

}
