<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Tabela extends Model
{
    protected $table = 'tabelas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadArquivo() {
        $arquivo = request()->file('arquivo');

        $path = 'assets/tabelas/';
        $name = str_slug(pathinfo($arquivo->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$arquivo->getClientOriginalExtension();

        $arquivo->move($path, $name);

        return $name;
    }
}
