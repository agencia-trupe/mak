<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('clientes_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ClienteCategoria', 'clientes_categoria_id');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'       => 190,
            'height'      => 110,
            'transparent' => true,
            'path'        => 'assets/img/clientes/'
        ]);
    }
}
