<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BlogComentario extends Model
{
    protected $table = 'blog_comentarios';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    public function scopePost($query, $id)
    {
        return $query->where('post_id', $id);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }
}
