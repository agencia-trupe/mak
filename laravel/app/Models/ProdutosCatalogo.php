<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutosCatalogo extends Model
{
    protected $table = 'produtos_catalogo';

    protected $guarded = ['id'];

    public static function uploadFile() {
        $file = request()->file('arquivo');

        $path = 'assets/produtos/catalogo/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }
}
