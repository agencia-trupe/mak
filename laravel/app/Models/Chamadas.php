<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Chamadas extends Model
{
    protected $table = 'chamadas';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'       => 360,
            'height'      => 85,
            'transparent' => true,
            'path'        => 'assets/img/chamadas/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'       => 360,
            'height'      => 85,
            'transparent' => true,
            'path'        => 'assets/img/chamadas/'
        ]);
    }

}
