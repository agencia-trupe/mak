<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrabalheConosco extends Model
{
    protected $table = 'trabalhe_conosco';

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function getCreatedAtOrderAttribute()
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $this->created_at)->format('Y-m-d');
    }

    public function scopeNaoLidos($query)
    {
        return $query->where('lido', '!=', 1);
    }

    public function countNaoLidos()
    {
        return $this->naoLidos()->count();
    }

    public static function uploadCurriculo() {
        $arquivo = request()->file('curriculo');

        $path = 'curriculos/';
        $name = str_slug(pathinfo($arquivo->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$arquivo->getClientOriginalExtension();

        $arquivo->move($path, $name);

        return $name;
    }
}
