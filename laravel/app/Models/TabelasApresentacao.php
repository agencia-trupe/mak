<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class TabelasApresentacao extends Model
{
    protected $table = 'tabelas_apresentacao';

    protected $guarded = ['id'];

}
