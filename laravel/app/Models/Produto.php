<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeLinha($query, $linha_id)
    {
        return $query->where('produtos_linha_id', $linha_id);
    }

    public function linha()
    {
        return $this->belongsTo('App\Models\ProdutoLinha', 'produtos_linha_id');
    }

    public function segmentos()
    {
        return $this->belongsToMany('App\Models\ProdutoSegmento', 'produto_segmento', 'produto_id', 'produto_segmento_id')->ordenados();
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 350,
                'height'  => null,
                'produto' => true,
                'path'    => 'assets/img/produtos/'
            ],
            [
                'width'   => 385,
                'height'  => 320,
                'bgcolor' => '#fff',
                'path'    => 'assets/img/produtos/destaque/'
            ],
        ]);
    }

    public function scopeDestaques($query)
    {
        return $query->where('destaque', 1);
    }
}
