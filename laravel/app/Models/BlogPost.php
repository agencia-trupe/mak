<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use App\Helpers\CropImage;
use Carbon\Carbon;

class BlogPost extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'blog_posts';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\BlogCategoria', 'categoria_id');
    }

    public function comentarios()
    {
        return $this->hasMany('App\Models\BlogComentario', 'post_id')->ordenados();
    }

    public function comentariosAprovados()
    {
        return $this->hasMany('App\Models\BlogComentario', 'post_id')->whereAprovado(1)->ordenados();
    }

    public function getComentariosParaAprovarAttribute()
    {
        return $this->comentarios()->where('aprovado', false)->count();
    }
}
