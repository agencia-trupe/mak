<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Marca extends Model
{
    protected $table = 'evolucao_da_marca';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'   => 286,
            'height'  => 154,
            'bgcolor' => '#ffffff',
            'path'    => 'assets/img/evolucao-da-marca/'
        ]);
    }
}
