<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Catalogo extends Model
{
    protected $table = 'evolucao_dos_catalogos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 190,
            'height' => null,
            'path'   => 'assets/img/evolucao-dos-catalogos/'
        ]);
    }
}
