<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Aplicacao extends Model
{
    protected $table = 'aplicacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 300,
                'height' => null,
                'path'   => 'assets/img/aplicacoes/thumbs/'
            ],
            [
                'width'  => 720,
                'height' => null,
                'path'   => 'assets/img/aplicacoes/'
            ],
        ]);
    }
}
