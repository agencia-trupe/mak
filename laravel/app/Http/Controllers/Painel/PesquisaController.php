<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Pesquisa;

class PesquisaController extends Controller
{
    public function index()
    {
        $contatosrecebidos = Pesquisa::orderBy('created_at', 'DESC')->get();

        return view('painel.contato.pesquisa-de-satisfacao.index', compact('contatosrecebidos'));
    }

    public function show(Pesquisa $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.pesquisa-de-satisfacao.show', compact('contato'));
    }

    public function destroy(Pesquisa $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.pesquisa-de-satisfacao.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(Pesquisa $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.contato.pesquisa-de-satisfacao.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
