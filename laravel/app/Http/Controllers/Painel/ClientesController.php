<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClientesRequest;
use App\Http\Controllers\Controller;

use App\Models\Cliente;
use App\Models\ClienteCategoria;

class ClientesController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = ClienteCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (ClienteCategoria::find($filtro)) {
            $registros = Cliente::where('clientes_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = Cliente::leftJoin('clientes_categorias as cat', 'cat.id', '=', 'clientes_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('clientes.*')
                ->ordenados()->get();
        }

        return view('painel.clientes.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.clientes.create', compact('categorias'));
    }

    public function store(ClientesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Cliente::upload_imagem();

            Cliente::create($input);

            return redirect()->route('painel.clientes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Cliente $registro)
    {
        $categorias = $this->categorias;

        return view('painel.clientes.edit', compact('registro', 'categorias'));
    }

    public function update(ClientesRequest $request, Cliente $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Cliente::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.clientes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Cliente $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.clientes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
