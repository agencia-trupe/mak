<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\BlogPostsRequest;
use App\Http\Controllers\Controller;

use App\Models\BlogPost;
use App\Models\BlogCategoria;

class BlogPostsController extends Controller
{
    public function __construct()
    {
        view()->share([
            'categorias' => BlogCategoria::ordenados()->lists('titulo', 'id')
        ]);
    }

    public function index()
    {
        $registros = BlogPost::ordenados()->paginate(10);

        return view('painel.blog.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.blog.create');
    }

    public function store(BlogPostsRequest $request)
    {
        try {

            $input = $request->all();

            BlogPost::create($input);
            return redirect()->route('painel.blog.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(BlogPost $registro)
    {
        return view('painel.blog.edit', compact('registro'));
    }

    public function update(BlogPostsRequest $request, BlogPost $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);
            return redirect()->route('painel.blog.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(BlogPost $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.blog.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
