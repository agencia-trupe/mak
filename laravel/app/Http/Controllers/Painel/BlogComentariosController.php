<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\BlogPost;
use App\Models\BlogComentario;

class BlogComentariosController extends Controller
{
    public function index(BlogPost $post)
    {
        $registros = $post->comentarios;

        return view('painel.blog.comentarios', compact('post', 'registros'));
    }

    public function destroy(BlogPost $post, BlogComentario $comentario)
    {
        try {

            $comentario->delete();
            return redirect()->back()->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function aprovacao(BlogPost $post, BlogComentario $comentario)
    {
        try {

            $comentario->update([
                'aprovado' => !$comentario->aprovado
            ]);
            return redirect()->back()->with('success', 'Status atualizado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao atualizar status: '.$e->getMessage()]);

        }
    }
}
