<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CasesEObrasRequest;
use App\Http\Controllers\Controller;

use App\Models\CaseObra;

class CasesEObrasController extends Controller
{
    public function index()
    {
        $registros = CaseObra::ordenados()->get();

        return view('painel.cases-e-obras.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cases-e-obras.create');
    }

    public function store(CasesEObrasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = CaseObra::upload_capa();

            CaseObra::create($input);

            return redirect()->route('painel.cases-e-obras.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(CaseObra $registro)
    {
        return view('painel.cases-e-obras.edit', compact('registro'));
    }

    public function update(CasesEObrasRequest $request, CaseObra $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = CaseObra::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.cases-e-obras.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(CaseObra $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.cases-e-obras.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
