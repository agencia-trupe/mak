<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AvisoHomeRequest;
use App\Http\Controllers\Controller;

use App\Models\AvisoHome;

class AvisoHomeController extends Controller
{
    public function index()
    {
        $registro = AvisoHome::first();

        return view('painel.aviso-home.edit', compact('registro'));
    }

    public function update(AvisoHomeRequest $request, AvisoHome $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.aviso-home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
