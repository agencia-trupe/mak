<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CertificadosEHomologacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Certificado;

class CertificadosEHomologacoesController extends Controller
{
    public function index()
    {
        $registros = Certificado::ordenados()->get();

        return view('painel.certificados-e-homologacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.certificados-e-homologacoes.create');
    }

    public function store(CertificadosEHomologacoesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Certificado::upload_imagem();

            Certificado::create($input);

            return redirect()->route('painel.certificados-e-homologacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Certificado $registro)
    {
        return view('painel.certificados-e-homologacoes.edit', compact('registro'));
    }

    public function update(CertificadosEHomologacoesRequest $request, Certificado $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Certificado::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.certificados-e-homologacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Certificado $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.certificados-e-homologacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
