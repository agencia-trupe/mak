<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CasesEObrasImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\CaseObra;
use App\Models\CaseObraImagem;

use App\Helpers\CropImage;

class CasesEObrasImagensController extends Controller
{
    public function index(CaseObra $registro)
    {
        $imagens = CaseObraImagem::caseobra($registro->id)->ordenados()->get();

        return view('painel.cases-e-obras.imagens.index', compact('imagens', 'registro'));
    }

    public function show(CaseObra $registro, CaseObraImagem $imagem)
    {
        return $imagem;
    }

    public function store(CaseObra $registro, CasesEObrasImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CaseObraImagem::uploadImagem();
            $input['caseobra_id'] = $registro->id;

            $imagem = CaseObraImagem::create($input);

            $view = view('painel.cases-e-obras.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(CaseObra $registro, CaseObraImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.cases-e-obras.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(CaseObra $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.cases-e-obras.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
