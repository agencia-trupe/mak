<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AplicacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Aplicacao;

class AplicacoesController extends Controller
{
    public function index()
    {
        $registros = Aplicacao::ordenados()->get();

        return view('painel.aplicacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.aplicacoes.create');
    }

    public function store(AplicacoesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Aplicacao::upload_imagem();

            Aplicacao::create($input);

            return redirect()->route('painel.aplicacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Aplicacao $registro)
    {
        return view('painel.aplicacoes.edit', compact('registro'));
    }

    public function update(AplicacoesRequest $request, Aplicacao $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Aplicacao::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.aplicacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aplicacao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.aplicacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
