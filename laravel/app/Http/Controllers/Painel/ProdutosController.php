<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoLinha;
use App\Models\ProdutoSegmento;

class ProdutosController extends Controller
{
    private $linhas;

    private $segmentos;

    public function __construct()
    {
        $this->linhas = ProdutoLinha::ordenados()->lists('titulo', 'id');
        $this->segmentos = ProdutoSegmento::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $linhas = $this->linhas;
        $filtro     = $request->query('filtro');

        if (ProdutoLinha::find($filtro)) {
            $registros = Produto::where('produtos_linha_id', $filtro)->ordenados()->get();
        } else {
            $registros = Produto::leftJoin('produtos_linhas as cat', 'cat.id', '=', 'produtos_linha_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('produtos.*')
                ->ordenados()->get();
        }

        return view('painel.produtos.index', compact('linhas', 'registros', 'filtro'));
    }

    public function create()
    {
        $linhas = $this->linhas;

        $segmentos = $this->segmentos;

        return view('painel.produtos.create', compact('linhas', 'segmentos'));
    }

    public function store(ProdutosRequest $request)
    {
        try {

            $input = $request->except('segmentos');
            $segmentos  = ($request->get('segmentos') ?: []);

            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();

            $registro = Produto::create($input);
            $registro->segmentos()->sync($segmentos);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Produto $registro)
    {
        $linhas = $this->linhas;

        $segmentos = $this->segmentos;

        return view('painel.produtos.edit', compact('registro', 'linhas', 'segmentos'));
    }

    public function update(ProdutosRequest $request, Produto $registro)
    {
        try {

            $input = $request->except('segmentos');

            if (isset($input['imagem'])) $input['imagem'] = Produto::upload_imagem();

            $registro->update($input);

            $segmentos = ($request->get('segmentos') ?: []);
            $registro->segmentos()->sync($segmentos);

            return redirect()->route('painel.produtos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Produto $registro)
    {
        try {

            $registro->segmentos()->detach();
            $registro->delete();

            return redirect()->route('painel.produtos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
