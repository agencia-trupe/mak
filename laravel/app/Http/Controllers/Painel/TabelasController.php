<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TabelasRequest;
use App\Http\Controllers\Controller;

use App\Models\Tabela;

class TabelasController extends Controller
{
    public function index()
    {
        $registros = Tabela::ordenados()->get();

        return view('painel.tabelas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.tabelas.create');
    }

    public function store(TabelasRequest $request)
    {
        try {

            $input = $request->all();

            $input['arquivo'] = Tabela::uploadArquivo();

            Tabela::create($input);

            return redirect()->route('painel.tabelas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Tabela $registro)
    {
        return view('painel.tabelas.edit', compact('registro'));
    }

    public function update(TabelasRequest $request, Tabela $registro)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = Tabela::uploadArquivo();
            }

            $registro->update($input);

            return redirect()->route('painel.tabelas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Tabela $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.tabelas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
