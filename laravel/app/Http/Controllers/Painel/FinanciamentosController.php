<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FinanciamentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Financiamento;

class FinanciamentosController extends Controller
{
    public function index()
    {
        $registros = Financiamento::ordenados()->get();

        return view('painel.financiamentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.financiamentos.create');
    }

    public function store(FinanciamentosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Financiamento::upload_imagem();

            Financiamento::create($input);

            return redirect()->route('painel.financiamentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Financiamento $registro)
    {
        return view('painel.financiamentos.edit', compact('registro'));
    }

    public function update(FinanciamentosRequest $request, Financiamento $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Financiamento::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.financiamentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Financiamento $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.financiamentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
