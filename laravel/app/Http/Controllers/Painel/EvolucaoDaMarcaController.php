<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EvolucaoDaMarcaRequest;
use App\Http\Controllers\Controller;

use App\Models\Marca;

class EvolucaoDaMarcaController extends Controller
{
    public function index()
    {
        $registros = Marca::ordenados()->get();

        return view('painel.evolucao-da-marca.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.evolucao-da-marca.create');
    }

    public function store(EvolucaoDaMarcaRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Marca::upload_imagem();

            Marca::create($input);

            return redirect()->route('painel.evolucao-da-marca.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Marca $registro)
    {
        return view('painel.evolucao-da-marca.edit', compact('registro'));
    }

    public function update(EvolucaoDaMarcaRequest $request, Marca $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Marca::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.evolucao-da-marca.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Marca $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.evolucao-da-marca.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
