<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EvolucaoDosCatalogosRequest;
use App\Http\Controllers\Controller;

use App\Models\Catalogo;

class EvolucaoDosCatalogosController extends Controller
{
    public function index()
    {
        $registros = Catalogo::ordenados()->get();

        return view('painel.evolucao-dos-catalogos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.evolucao-dos-catalogos.create');
    }

    public function store(EvolucaoDosCatalogosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Catalogo::upload_imagem();

            Catalogo::create($input);

            return redirect()->route('painel.evolucao-dos-catalogos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Catalogo $registro)
    {
        return view('painel.evolucao-dos-catalogos.edit', compact('registro'));
    }

    public function update(EvolucaoDosCatalogosRequest $request, Catalogo $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Catalogo::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.evolucao-dos-catalogos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Catalogo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.evolucao-dos-catalogos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
