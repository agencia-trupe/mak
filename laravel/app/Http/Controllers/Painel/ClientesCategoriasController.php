<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClientesCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\ClienteCategoria;

class ClientesCategoriasController extends Controller
{
    public function index()
    {
        $categorias = ClienteCategoria::ordenados()->get();

        return view('painel.clientes.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.clientes.categorias.create');
    }

    public function store(ClientesCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            ClienteCategoria::create($input);
            return redirect()->route('painel.clientes.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(ClienteCategoria $categoria)
    {
        return view('painel.clientes.categorias.edit', compact('categoria'));
    }

    public function update(ClientesCategoriasRequest $request, ClienteCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.clientes.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(ClienteCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.clientes.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
