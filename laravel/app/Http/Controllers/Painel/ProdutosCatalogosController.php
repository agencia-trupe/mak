<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosCatalogosRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCatalogo;

class ProdutosCatalogosController extends Controller
{
    public function index()
    {
        $registros = ProdutoCatalogo::ordenados()->get();

        return view('painel.produtos-catalogos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.produtos-catalogos.create');
    }

    public function store(ProdutosCatalogosRequest $request)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = ProdutoCatalogo::uploadFile();
            }

            ProdutoCatalogo::create($input);

            return redirect()->route('painel.produtos.catalogos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoCatalogo $registro)
    {
        return view('painel.produtos-catalogos.edit', compact('registro'));
    }

    public function update(ProdutosCatalogosRequest $request, ProdutoCatalogo $registro)
    {
        try {

            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = ProdutoCatalogo::uploadFile();
            }

            $registro->update($input);

            return redirect()->route('painel.produtos.catalogos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoCatalogo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.catalogos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
