<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosSegmentosRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoSegmento;

class ProdutosSegmentosController extends Controller
{
    public function index()
    {
        $segmentos = ProdutoSegmento::ordenados()->get();

        return view('painel.produtos.segmentos.index', compact('segmentos'));
    }

    public function create()
    {
        return view('painel.produtos.segmentos.create');
    }

    public function store(ProdutosSegmentosRequest $request)
    {
        try {

            $input = $request->all();

            ProdutoSegmento::create($input);
            return redirect()->route('painel.produtos.segmentos.index')->with('success', 'Segmento adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar segmento: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoSegmento $segmento)
    {
        return view('painel.produtos.segmentos.edit', compact('segmento'));
    }

    public function update(ProdutosSegmentosRequest $request, ProdutoSegmento $segmento)
    {
        try {

            $input = $request->all();

            $segmento->update($input);
            return redirect()->route('painel.produtos.segmentos.index')->with('success', 'Segmento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar segmento: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoSegmento $segmento)
    {
        try {

            $segmento->delete();
            return redirect()->route('painel.produtos.segmentos.index')->with('success', 'Segmento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir segmento: '.$e->getMessage()]);

        }
    }
}
