<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TabelasApresentacaoRequest;
use App\Http\Controllers\Controller;

use App\Models\TabelasApresentacao;

class TabelasApresentacaoController extends Controller
{
    public function index()
    {
        $registro = TabelasApresentacao::first();

        return view('painel.tabelas-apresentacao.edit', compact('registro'));
    }

    public function update(TabelasApresentacaoRequest $request, TabelasApresentacao $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.tabelas-apresentacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
