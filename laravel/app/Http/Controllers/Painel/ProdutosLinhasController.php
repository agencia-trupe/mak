<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosLinhasRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoLinha;

class ProdutosLinhasController extends Controller
{
    public function index()
    {
        $linhas = ProdutoLinha::ordenados()->get();

        return view('painel.produtos.linhas.index', compact('linhas'));
    }

    public function create()
    {
        return view('painel.produtos.linhas.create');
    }

    public function store(ProdutosLinhasRequest $request)
    {
        try {

            $input = $request->all();

            ProdutoLinha::create($input);
            return redirect()->route('painel.produtos.linhas.index')->with('success', 'Linha adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar linha: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoLinha $linha)
    {
        return view('painel.produtos.linhas.edit', compact('linha'));
    }

    public function update(ProdutosLinhasRequest $request, ProdutoLinha $linha)
    {
        try {

            $input = $request->all();

            $linha->update($input);
            return redirect()->route('painel.produtos.linhas.index')->with('success', 'Linha alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar linha: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoLinha $linha)
    {
        try {

            $linha->delete();
            return redirect()->route('painel.produtos.linhas.index')->with('success', 'Linha excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir linha: '.$e->getMessage()]);

        }
    }
}
