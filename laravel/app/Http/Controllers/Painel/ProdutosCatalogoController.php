<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosCatalogoRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutosCatalogo;

class ProdutosCatalogoController extends Controller
{
    public function index()
    {
        $registro = ProdutosCatalogo::first();

        return view('painel.produtos-catalogo.edit', compact('registro'));
    }

    public function update(ProdutosCatalogoRequest $request, ProdutosCatalogo $registro)
    {
        try {
            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = ProdutosCatalogo::uploadFile();
            }

            $registro->update($input);

            return redirect()->route('painel.produtos.catalogo.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
