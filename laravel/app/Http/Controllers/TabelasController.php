<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\TabelasApresentacao;
use App\Models\Tabela;

class TabelasController extends Controller
{
    public function index()
    {
        $apresentacao = TabelasApresentacao::first();
        $tabelas      = Tabela::ordenados()->get();

        return view('frontend.tabelas', compact('apresentacao', 'tabelas'));
    }
}
