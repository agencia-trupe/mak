<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Servico;

class ServicosController extends Controller
{
    public function index(Servico $servico)
    {
        $servicos = Servico::ordenados()->get(['slug', 'titulo']);

        if (!$servico->exists) {
            $servico = Servico::ordenados()->first();
        }

        return view('frontend.servicos', compact('servicos', 'servico'));
    }
}
