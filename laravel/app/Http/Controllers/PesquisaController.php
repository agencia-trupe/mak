<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\PesquisaRequest;

use App\Models\Pesquisa;
use App\Models\Contato;

class PesquisaController extends Controller
{
    public function index()
    {
        return view('frontend.pesquisa-de-satisfacao');
    }

    public function post(PesquisaRequest $request)
    {
        try {

            Pesquisa::create($request->all());

            $contato = Contato::first();

            if (isset($contato->email)) {
                \Mail::send('emails.pesquisa', $request->all(), function($message) use ($request, $contato)
                {
                    $message->to($contato->email, config('site.name'))
                            ->subject('[PESQUISA DE SATISFAÇÃO] '.config('site.name'))
                            ->replyTo($request->get('email'), $request->get('nome'));
                });
            }

            return redirect()->route('pesquisa')->with('enviado', true);

        } catch (\Exception $e) {

            dd($e);
            return redirect()->route('pesquisa')->withInput()->withErrors(['Ocorreu um erro. Tente novamente.']);

        }
    }
}
