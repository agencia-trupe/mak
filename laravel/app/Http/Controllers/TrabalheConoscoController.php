<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\TrabalheConoscoRequest;

use App\Models\TrabalheConosco;
use App\Models\Contato;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        return view('frontend.trabalhe-conosco');
    }

    public function post(TrabalheConoscoRequest $request)
    {
        try {

            $input = $request->except('curriculo');
            $input['curriculo'] = TrabalheConosco::uploadCurriculo();

            TrabalheConosco::create($input);

            $contato = Contato::first();

            if (isset($contato->email)) {
                \Mail::send('emails.trabalhe', $input, function($message) use ($request, $contato)
                {
                    $message->to($contato->email, config('site.name'))
                            ->subject('[TRABALHE CONOSCO] '.config('site.name'))
                            ->replyTo($request->get('email'), $request->get('nome'));
                });
            }

            return redirect()->route('trabalhe')->with('enviado', true);

        } catch (\Exception $e) {

            return redirect()->route('trabalhe')->withInput()->withErrors(['Ocorreu um erro. Tente novamente.']);

        }
    }
}
