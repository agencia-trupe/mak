<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\BlogComentariosRequest;

use App\Models\BlogPost;
use App\Models\BlogCategoria;

use App\Models\Contato;

class BlogController extends Controller
{
    public function __construct()
    {
        $categorias = BlogCategoria::ordenados()->get();
        $arquivos   = BlogPost::select(\DB::raw('YEAR(data) as ano, MONTH(data) as mes'))
                               ->groupBy('ano', 'mes')
                               ->orderBy('ano', 'DESC')
                               ->orderBy('mes', 'DESC')
                               ->get();

        view()->share('categorias', $categorias);
        view()->share('arquivos', $arquivos);
    }

    public function arquivo($ano = null, $mes = null)
    {
        if (!$ano || !$mes) abort('404');

        $posts = BlogPost::ordenados()
                   ->whereRaw("YEAR(data) = $ano AND MONTH(data) = $mes")
                   ->get();

        return view('frontend.blog.lista', compact('posts', 'ano', 'mes'));
    }

    public function categoria(BlogCategoria $categoria)
    {
        $posts = $categoria->posts;

        return view('frontend.blog.lista', compact('posts', 'categoria'));
    }

    public function index(BlogPost $post)
    {
        if (!$post->exists) {
            $post = BlogPost::ordenados()->first();
        }

        $data_post = \Carbon\Carbon::createFromFormat('d/m/Y', $post->data)->format('Y-m-d');

        $anterior = BlogPost::select('slug')
                          ->where('data', '<', $data_post)
                          ->orWhere(function($query) use ($data_post, $post) {
                              $query->where('data', $data_post)
                                    ->where('id', '<', $post->id);
                          })
                          ->orderBy('data', 'DESC')
                          ->orderBy('id', 'DESC')
                          ->first();

        $proximo = BlogPost::select('slug')
                          ->where('data', '>', $data_post)
                          ->orWhere(function($query) use ($data_post, $post) {
                              $query->where('data', $data_post)
                                    ->where('id', '>', $post->id);
                          })
                          ->orderBy('data', 'ASC')
                          ->orderBy('id', 'ASC')
                          ->first();

        $data = [
            'post'     => $post,
            'proximo'  => $proximo ? $proximo->slug : null,
            'anterior' => $anterior ? $anterior->slug : null
        ];

        return view('frontend.blog.index', $data);
    }

    public function comentario(BlogComentariosRequest $request)
    {
        try {

            $post = BlogPost::findOrFail($request->get('post'));
            $post->comentarios()->create($request->except('post'));

            $contato = Contato::first();

            if (isset($contato->email)) {
                \Mail::send('emails.comentario', [
                    'comentario' => $request->all(),
                    'post'       => $post
                ], function($message) use ($request, $contato) {
                    $message->to($contato->email, config('site.name'))
                            ->subject('[NOVO COMENTÁRIO] '.config('site.name'));
                });
            }

        } catch (\Exception $e) {
            $response = [
                'message' => 'Ocorreu um erro. Tente novamente.'
            ];

            return response()->json($response);

        }

        $response = [
            'success' => true,
            'message' => 'Comentário enviado com sucesso. Sujeito à aprovação da moderação.'
        ];

        return response()->json($response);
    }
}
