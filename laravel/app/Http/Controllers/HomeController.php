<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AvisoHome;
use App\Models\Banner;
use App\Models\Produto;
use App\Models\Chamadas;
use App\Models\BlogPost;
use App\Models\Financiamento;

class HomeController extends Controller
{
    public function index()
    {
        $aviso          = AvisoHome::first();
        $banners        = Banner::ordenados()->get();
        $produtos       = Produto::ordenados()->destaques()->get();
        $chamadas       = Chamadas::first();
        $novidades      = BlogPost::ordenados()->take(2)->get();
        $financiamentos = Financiamento::ordenados()->get();

        return view('frontend.home', compact('aviso', 'banners', 'produtos', 'chamadas', 'novidades', 'financiamentos'));
    }
}
