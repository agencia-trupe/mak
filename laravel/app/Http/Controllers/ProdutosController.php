<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ProdutoLinha;
use App\Models\ProdutoSegmento;
use App\Models\Produto;
use App\Models\ProdutoCatalogo;
use App\Models\Contato;
use App\Models\Aplicacao;

class ProdutosController extends Controller
{
    public function index(ProdutoLinha $linha)
    {
        $linhas    = ProdutoLinha::ordenados()->get();
        $segmentos = ProdutoSegmento::ordenados()->get();

        if (!$linha->exists) {
            $linha = ProdutoLinha::ordenados()->first();
        }

        $produtos = $linha->produtos;

        $catalogos = ProdutoCatalogo::ordenados()->get();

        return view('frontend.produtos.index', compact('linhas', 'segmentos', 'linha', 'produtos', 'catalogos'));
    }

    public function segmento(ProdutoSegmento $segmento)
    {
        $linhas    = ProdutoLinha::ordenados()->get();
        $segmentos = ProdutoSegmento::ordenados()->get();

        if (!$segmento->exists) {
            $segmento = ProdutoLinha::ordenados()->first();
        }

        $produtos = $segmento->produtos;

        $catalogos = ProdutoCatalogo::ordenados()->get();

        return view('frontend.produtos.index', compact('linhas', 'segmentos', 'segmento', 'produtos', 'catalogos'));
    }

    public function show(ProdutoLinha $linha, Produto $produto)
    {
        $linhas    = ProdutoLinha::ordenados()->get();
        $segmentos = ProdutoSegmento::ordenados()->get();

        $linha     = $produto->linha;

        $catalogos = ProdutoCatalogo::ordenados()->get();

        return view('frontend.produtos.show', compact('linhas', 'segmentos', 'linha', 'produto', 'catalogos'));
    }

    public function catalogo($id)
    {
        $catalogo = ProdutoCatalogo::findOrFail($id);

        return view('frontend.produtos.catalogo', compact('catalogo'));
    }

    public function catalogoPost(Request $request)
    {
        $this->validate($request, [
            'id'       => 'required',
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required'
        ]);

        $catalogo = ProdutoCatalogo::findOrFail($request->get('id'));

        $input = $request->all();
        $input['catalogo'] = $catalogo->nome;

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.catalogo', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CATÁLOGO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return response()->json([
            'link' => url('assets/produtos/catalogo/'.$catalogo->arquivo)
        ]);
    }

    public function aplicacoes($id = null)
    {
        $linhas    = ProdutoLinha::ordenados()->get();
        $segmentos = ProdutoSegmento::ordenados()->get();
        $catalogos = ProdutoCatalogo::ordenados()->get();

        if (!$id) {
            $aplicacoes = Aplicacao::ordenados()->get();

            return view('frontend.produtos.aplicacoes.index', compact('linhas', 'segmentos', 'catalogos', 'aplicacoes'));
        }

        $aplicacao = Aplicacao::find($id);

        if (!$aplicacao) abort('404');

        return view('frontend.produtos.aplicacoes.show', compact('linhas', 'segmentos', 'catalogos', 'aplicacao'));
    }
}
