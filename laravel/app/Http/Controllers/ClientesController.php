<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ClienteCategoria;
use App\Models\Cliente;

class ClientesController extends Controller
{
    public function index(ClienteCategoria $categoria)
    {
        $categorias = ClienteCategoria::ordenados()->get();

        if (!$categoria->exists) {
            $categoria = ClienteCategoria::ordenados()->first();
        }

        return view('frontend.clientes', compact('categorias', 'categoria'));
    }
}
