<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\CaseObra;

class CasesController extends Controller
{
    public function index(CaseObra $case)
    {
        if (!$case->exists) {
            $cases = CaseObra::ordenados()->get();
            return view('frontend.cases.index', compact('cases'));
        }

        return view('frontend.cases.show', compact('case'));
    }
}
