<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Empresa;
use App\Models\Marca;
use App\Models\Catalogo;
use App\Models\Certificado;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa      = Empresa::first();
        $marcas       = Marca::ordenados()->get();
        $catalogos    = Catalogo::ordenados()->get();
        $certificados = Certificado::ordenados()->get();

        return view('frontend.empresa', compact('empresa', 'marcas', 'catalogos', 'certificados'));
    }
}
