<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('produtos/aplicacoes/{id?}', 'ProdutosController@aplicacoes')->name('produtos.aplicacoes');
    // Route::get('produtos/segmento/{produtos_segmento}', 'ProdutosController@segmento')->name('produtos.segmento');
    Route::get('produtos/{produtos_linha?}', 'ProdutosController@index')->name('produtos');
    Route::get('produtos/{produtos_linha}/{produto_slug}', 'ProdutosController@show')->name('produtos.show');
    Route::get('servicos/{servico_slug?}', 'ServicosController@index')->name('servicos');
    Route::get('cases-e-obras/{case_slug?}', 'CasesController@index')->name('cases');
    Route::get('clientes/{clientes_categoria?}', 'ClientesController@index')->name('clientes');
    Route::get('tabelas', 'TabelasController@index')->name('tabelas');
    Route::get('blog/{post_slug?}', 'BlogController@index')->name('blog');
    Route::get('blog/c/{blog_categoria}', 'BlogController@categoria')->name('blog.categoria');
    Route::get('blog/a/{ano}/{mes}', 'BlogController@arquivo')->name('blog.arquivo');
    Route::post('blog-comentario', 'BlogController@comentario');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('pesquisa-de-satisfacao', 'PesquisaController@index')->name('pesquisa');
    Route::post('pesquisa-de-satisfacao', 'PesquisaController@post')->name('pesquisa.post');
    Route::get('trabalhe-conosco', 'TrabalheConoscoController@index')->name('trabalhe');
    Route::post('trabalhe-conosco', 'TrabalheConoscoController@post')->name('trabalhe.post');
    Route::get('catalogo/{id}', 'ProdutosController@catalogo')->name('catalogo');
    Route::post('catalogo', 'ProdutosController@catalogoPost')->name('catalogo.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('aplicacoes', 'AplicacoesController');
		Route::resource('certificados-e-homologacoes', 'CertificadosEHomologacoesController');
		Route::resource('aviso-home', 'AvisoHomeController', ['only' => ['index', 'update']]);
        Route::resource('cases-e-obras', 'CasesEObrasController');
        Route::get('cases-e-obras/{cases_e_obras}/imagens/clear', [
            'as'   => 'painel.cases-e-obras.imagens.clear',
            'uses' => 'CasesEObrasImagensController@clear'
        ]);
        Route::resource('cases-e-obras.imagens', 'CasesEObrasImagensController', ['parameters' => ['imagens' => 'imagens_cases-e-obras']]);
		Route::resource('produtos/catalogos', 'ProdutosCatalogosController');
		//Route::resource('produtos/segmentos', 'ProdutosSegmentosController', ['parameters' => ['segmentos' => 'segmentos_produtos']]);
		Route::resource('produtos/linhas', 'ProdutosLinhasController', ['parameters' => ['linhas' => 'linhas_produtos']]);
		Route::resource('produtos', 'ProdutosController');
        Route::resource('blog/categorias', 'BlogCategoriasController');
        Route::resource('blog', 'BlogPostsController');
        Route::get('blog/{blog}/comentarios/{comentarios}/aprovacao', 'BlogComentariosController@aprovacao')->name('painel.blog.comentarios.aprovacao');
        Route::resource('blog.comentarios', 'BlogComentariosController');
		Route::resource('tabelas-apresentacao', 'TabelasApresentacaoController', ['only' => ['index', 'update']]);
		Route::resource('tabelas', 'TabelasController');
		Route::resource('clientes/categorias', 'ClientesCategoriasController', ['parameters' => ['categorias' => 'categorias_clientes']]);
		Route::resource('clientes', 'ClientesController');
		Route::resource('servicos', 'ServicosController');
		Route::resource('evolucao-dos-catalogos', 'EvolucaoDosCatalogosController');
		Route::resource('evolucao-da-marca', 'EvolucaoDaMarcaController');
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('chamadas', 'ChamadasController', ['only' => ['index', 'update']]);
		Route::resource('financiamentos', 'FinanciamentosController');
		Route::resource('banners', 'BannersController');

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::get('contato/trabalhe-conosco/{trabalhe_conosco}/toggle', ['as' => 'painel.contato.trabalhe-conosco.toggle', 'uses' => 'TrabalheConoscoController@toggle']);
        Route::resource('contato/trabalhe-conosco', 'TrabalheConoscoController');
        Route::get('contato/pesquisa-de-satisfacao/{pesquisa_de_satisfacao}/toggle', ['as' => 'painel.contato.pesquisa-de-satisfacao.toggle', 'uses' => 'PesquisaController@toggle']);
        Route::resource('contato/pesquisa-de-satisfacao', 'PesquisaController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
