<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosCatalogoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'arquivo' => 'required|mimes:pdf',
        ];
    }
}
