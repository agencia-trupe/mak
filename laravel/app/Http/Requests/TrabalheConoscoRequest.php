<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TrabalheConoscoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'      => 'required',
            'email'     => 'required|email',
            'cargo'     => 'required',
            'curriculo' => 'required|mimes:pdf,doc,docx'
        ];
    }

    public function messages() {
        return [
            'nome.required'      => 'Preencha seu nome',
            'email.required'     => 'Preencha seu e-mail',
            'cargo.required'     => 'Preencha sua área de atuação / cargo pretendido',
            'email.email'        => 'Insira um endereço de e-mail válido',
            'curriculo.required' => 'Anexe seu currículo',
            'curriculo.mimes'    => 'Currículo deve ser um arquivo PDF ou DOC'
        ];
    }
}
