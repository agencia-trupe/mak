<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CasesEObrasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'capa' => 'required|image',
            'descricao' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
