<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EvolucaoDaMarcaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'ano' => 'required',
            'imagem' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
