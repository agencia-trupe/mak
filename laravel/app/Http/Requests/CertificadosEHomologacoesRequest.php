<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CertificadosEHomologacoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'imagem' => 'required|image',
            'texto' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
