<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produtos_linha_id' => 'required',
            'titulo' => 'required',
            'imagem' => 'required|image',
            'resumo' => 'required',
            'descricao' => 'required',
            'corrente_nominal_do_barramento_principal' => '',
            'corrente_suportavel_de_curta_duracao' => '',
            'corrente_suportavel_de_crista' => '',
            'grau_de_protecao' => '',
            'cor_padrao' => '',
            'tipo_de_construcao' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
