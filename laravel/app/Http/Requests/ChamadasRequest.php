<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo_1' => 'required',
            'imagem_1' => 'required|image',
            'texto_1' => 'required',
            'link_1' => 'required',
            'titulo_2' => 'required',
            'imagem_2' => 'required|image',
            'texto_2' => 'required',
            'link_2' => 'required',
        ];
    }
}
