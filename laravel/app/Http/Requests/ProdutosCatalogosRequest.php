<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosCatalogosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'arquivo' => 'required|mimes:pdf',
        ];

        if ($this->method() != 'POST') {
            $rules['arquivo'] = 'mimes:pdf';
        }

        return $rules;
    }
}
