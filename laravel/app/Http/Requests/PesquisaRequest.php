<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PesquisaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'empresa'                    => 'required',
            'contato'                    => 'required',
            'email'                      => 'required|email',
            'departamento'               => 'required',
            'pedido'                     => 'required',
            'atendimento_comercial'      => 'required',
            'projetos_e_suporte_tecnico' => 'required',
            'qualidade_do_produto'       => 'required',
            'pos_venda'                  => 'required',
            'indicaria'                  => 'required',
        ];
    }

    public function messages() {
        return [
            'empresa.required'                    => 'Preencha sua empresa',
            'contato.required'                    => 'Preencha seu contato',
            'email.required'                      => 'Preencha seu e-mail',
            'departamento.required'               => 'Preencha seu departamento',
            'pedido.required'                     => 'Preencha seu número de pedido ou obra',
            'email.email'                         => 'Insira um endereço de e-mail válido',
            'atendimento_comercial.required'      => 'Responda a pesquisa sobre Atendimento Comercial',
            'projetos_e_suporte_tecnico.required' => 'Responda a pesquisa sobre Projetos e suporte técnico',
            'qualidade_do_produto.required'       => 'Responda a pesquisa sobre Qualidade do produto',
            'pos_venda.required'                  => 'Responda a pesquisa sobre Pós-venda',
            'indicaria.required'                  => 'Responda a pesquisa sobre Indicação da Mak Painéis',
        ];
    }
}
