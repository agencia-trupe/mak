<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BlogComentariosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'       => 'required',
            'email'      => 'required|email',
            'comentario' => 'required',
            'post'       => 'required'
        ];
    }
}
