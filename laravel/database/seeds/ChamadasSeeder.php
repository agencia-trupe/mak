<?php

use Illuminate\Database\Seeder;

class ChamadasSeeder extends Seeder
{
    public function run()
    {
        DB::table('chamadas')->insert([
            'titulo_1' => '',
            'imagem_1' => '',
            'texto_1' => '',
            'link_1' => '',
            'titulo_2' => '',
            'imagem_2' => '',
            'texto_2' => '',
            'link_2' => '',
        ]);
    }
}
