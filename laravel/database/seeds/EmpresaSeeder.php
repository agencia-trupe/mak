<?php

use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresa')->insert([
            'texto' => '',
            'missao' => '',
            'visao' => '',
            'valores' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
        ]);
    }
}
