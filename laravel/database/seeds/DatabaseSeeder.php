<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(AvisoHomeSeeder::class);
		$this->call(ProdutosCatalogoSeeder::class);
		$this->call(TabelasApresentacaoSeeder::class);
		$this->call(EmpresaSeeder::class);
		$this->call(ChamadasSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
