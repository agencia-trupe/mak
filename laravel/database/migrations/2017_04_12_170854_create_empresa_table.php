<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('missao');
            $table->text('visao');
            $table->text('valores');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }
}
