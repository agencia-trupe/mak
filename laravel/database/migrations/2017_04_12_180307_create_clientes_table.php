<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    public function up()
    {
        Schema::create('clientes_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clientes_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('imagem');
            $table->foreign('clientes_categoria_id')->references('id')->on('clientes_categorias')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clientes');
        Schema::drop('clientes_categorias');
    }
}
