<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesEObrasTable extends Migration
{
    public function up()
    {
        Schema::create('cases_e_obras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('capa');
            $table->text('descricao');
            $table->timestamps();
        });

        Schema::create('cases_e_obras_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caseobra_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('caseobra_id')->references('id')->on('cases_e_obras')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('cases_e_obras_imagens');
        Schema::drop('cases_e_obras');
    }
}
