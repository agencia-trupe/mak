<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesquisasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesquisas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('empresa');
            $table->string('contato');
            $table->string('email');
            $table->string('telefone');
            $table->string('departamento');
            $table->string('pedido');
            $table->string('atendimento_comercial');
            $table->string('projetos_e_suporte_tecnico');
            $table->string('qualidade_do_produto');
            $table->string('pos_venda');
            $table->string('indicaria');
            $table->text('mensagem');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pesquisas');
    }
}
