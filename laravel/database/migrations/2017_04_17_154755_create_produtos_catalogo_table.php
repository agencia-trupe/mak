<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosCatalogoTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_catalogo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos_catalogo');
    }
}
