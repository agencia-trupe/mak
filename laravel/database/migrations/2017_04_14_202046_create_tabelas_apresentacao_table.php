<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelasApresentacaoTable extends Migration
{
    public function up()
    {
        Schema::create('tabelas_apresentacao', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tabelas_apresentacao');
    }
}
