<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostsTable extends Migration
{
    public function up()
    {
        Schema::create('blog_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('categoria_id')->unsigned()->nullable();
            $table->string('data');
            $table->string('titulo');
            $table->string('slug');
            $table->text('texto');
            $table->timestamps();

            $table->foreign('categoria_id')
                  ->references('id')
                  ->on('blog_categorias')
                  ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::drop('blog_posts');
    }
}
