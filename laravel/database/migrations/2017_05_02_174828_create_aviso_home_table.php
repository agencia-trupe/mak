<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvisoHomeTable extends Migration
{
    public function up()
    {
        Schema::create('aviso_home', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->boolean('ativo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aviso_home');
    }
}
