<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvolucaoDosCatalogosTable extends Migration
{
    public function up()
    {
        Schema::create('evolucao_dos_catalogos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('evolucao_dos_catalogos');
    }
}
