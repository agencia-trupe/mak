<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_linhas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produtos_linha_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('imagem');
            $table->text('resumo');
            $table->text('descricao');
            $table->boolean('destaque');
            $table->string('corrente_nominal_do_barramento_principal');
            $table->string('corrente_suportavel_de_curta_duracao');
            $table->string('corrente_suportavel_de_crista');
            $table->string('grau_de_protecao');
            $table->string('cor_padrao');
            $table->string('tipo_de_construcao');
            $table->foreign('produtos_linha_id')->references('id')->on('produtos_linhas')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('produtos_segmentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('produto_segmento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('produto_id')->unsigned();
            $table->integer('produto_segmento_id')->unsigned();
            $table->timestamps();
            $table->foreign('produto_id')->references('id')->on('produtos');
            $table->foreign('produto_segmento_id')->references('id')->on('produtos_segmentos');
        });
    }

    public function down()
    {
        Schema::drop('produto_segmento');
        Schema::drop('produtos_segmentos');
        Schema::drop('produtos');
        Schema::drop('produtos_linhas');
    }
}
