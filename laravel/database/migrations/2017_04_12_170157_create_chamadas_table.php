<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadasTable extends Migration
{
    public function up()
    {
        Schema::create('chamadas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo_1');
            $table->string('imagem_1');
            $table->text('texto_1');
            $table->string('link_1');
            $table->string('titulo_2');
            $table->string('imagem_2');
            $table->text('texto_2');
            $table->string('link_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('chamadas');
    }
}
