-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: mak
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `certificados_e_homologacoes`
--

DROP TABLE IF EXISTS `certificados_e_homologacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificados_e_homologacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificados_e_homologacoes`
--

LOCK TABLES `certificados_e_homologacoes` WRITE;
/*!40000 ALTER TABLE `certificados_e_homologacoes` DISABLE KEYS */;
INSERT INTO `certificados_e_homologacoes` VALUES (1,0,'ISO 9001:2008','iso_20170502213251.png','<p>A ISO 9001 &eacute; uma norma internacional que fornece requisitos para o Sistema de Gest&atilde;o da Qualidade (SGQ) das organiza&ccedil;&otilde;es. Faz parte de uma s&eacute;rie de normas publicadas pela ISO (International Organisation for Standardisation &ndash; Organiza&ccedil;&atilde;o Internacional de Normaliza&ccedil;&atilde;o), geralmente chamada no coletivo de &ldquo;s&eacute;rie ISO 9000&rdquo;&nbsp;</p>\r\n\r\n<p><strong>Quais seus objetivos?&nbsp;</strong></p>\r\n\r\n<p>O objetivo da ISO 9001 &eacute; fornecer um conjunto de requisitos visando: Atender &agrave;s necessidades e expectativas de seus clientes; e Estabelecer a conformidade com as leis e regulamentos aplic&aacute;veis.&nbsp;</p>\r\n\r\n<p><strong>Por que a Mak Implantou a ISO?</strong></p>\r\n\r\n<p>Os principais objetivos da Mak Pain&eacute;is &eacute; a qualidade e seguran&ccedil;a, sendo uma empresa comprometida com a gest&atilde;o da qualidade, sempre melhorando seus processos para satisfa&ccedil;&atilde;o dos clientes internos e externos. A Mak Pain&eacute;is se preocupa com a forma que &eacute; vista pelos clientes, deseja melhorar continuamente e est&aacute; aberta e sempre preparada a novas oportunidades e parceiras.</p>\r\n\r\n<p>Com este compromisso resultou no reconhecimento e conquista da certifica&ccedil;&atilde;o ISO 9001:2008.</p>\r\n\r\n<p><img src=\"http://mak.dev/assets/img/editor/image3_20170502213111.png\" /></p>\r\n','2017-05-02 21:31:14','2017-05-02 21:32:51'),(2,0,'fsd f sad as','img-20170214-121454_20170502214808.jpg','','2017-05-02 21:48:08','2017-05-02 21:48:08');
/*!40000 ALTER TABLE `certificados_e_homologacoes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-02 21:52:38
